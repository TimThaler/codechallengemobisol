class BaseCustomer < ApplicationRecord
  include ParamsExtendable
  has_secure_password
  belongs_to :extension, polymorphic: true
  validates :name, presence: true, uniqueness: true

  def self.extend_base_customer(col_name, type)
    ActiveRecord::Schema.define do 
      change_table :base_customers do |t|
        t.column col_name.to_sym, type.to_sym
      end
    end
  end

  def self.create_with_extension(attributes)
    sub = nil
    Extension.constants.select{|c| Extension.const_get(c).is_a? Class}.map do |ext|
      sub = "Extension::#{ext}".constantize.new
      if sub.attribute_names | self.extension_attributes_to_s(attributes) == sub.attribute_names
        self.extension_attributes_to_s(attributes).map do |key|
          sub.send("#{key}=", attributes[key.to_sym])
        end
        sub.save!
        sub.create_base_customer!(name: attributes[:name], phone: attributes[:phone],password: BCrypt::Password.create(attributes[:password]))
      end
    end
    sub
  end

  def self.extension_attributes_to_s(attr={})
    attr.except(:name,:phone, :password).keys.map(&:to_s)
  end
end

