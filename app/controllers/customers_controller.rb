class CustomersController < ApplicationController
  include Responsible
  include ParamsExtendable
  include Sessionable
  include SessionsHelper

  #before_action :http_auth
  before_action :current_user, only: [:index, :show, :update]

  def index
    @customers = BaseCustomer.includes(:extension).where(extension_type: @current_user.extension_type)
      .map{|c| c.as_json.merge(c.extension.as_json){|key,a,b|a}.except("created_at","updated_at", "password_digest")}
    #json_response @customers, 200
  end

  def show
    @customer = BaseCustomer.includes(:extension).find_by(extension_type: @current_user.extension_type)
  end

  def create
    @customer = BaseCustomer.new customer_params
    if @customer.save
      redirect_to customer_index_path
    else
      flash.now.alert = "Saving failed. Try again"
      redirect_to customer_new_path
    end
  end

  def new 
    @customer = BaseCustomer.new
  end

  def edit
    @customer = BaseCustomer.where(id: params[:id]).first
  end

  def update
    # update only spec attributes
    # not override them with empty param keys -as it would be now
    @customer.update_attributes params[:base_customer]
    @customer.extension.update_attribute extended_params
  end

  private 
  def extended_params
    attr = self.extension.attributes.keys - ['id','updated_at','created_at']
    params.require(:base_customer).permit(:name, :phone, :batt_cap, extensions: attr)
  end

  def customer_params
    params.require(:base_customer).permit(:name, :batt_cap, :phone, :password)
  end
end
