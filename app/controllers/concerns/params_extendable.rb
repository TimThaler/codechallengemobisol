module ParamsExtendable
  extend ActiveSupport::Concern

  def specific_params
    self.as_json.except("created_at", "updated_at", "password_digest")
  end
end
