module Authable
  extend ActiveSupport::Concern
  def http_auth
    authenticate_or_request_with_http_basic do |username,password|
     @base = BaseCustomer.find_by(name: username).try(:authenticate, password)
    end
  end
end
