module Sessionable 
  extend ActiveSupport::Concern

  def login customer
    session[:customer_id] = customer.id
  end

  def logout
    session.delete(:customer_id)
    @current_user = nil
  end
end
