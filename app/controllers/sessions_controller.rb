class SessionsController < ApplicationController
  include Sessionable
  
  def create
   # authenticate_or_request_with_http_basic do |username,password|
     customer = BaseCustomer.find_by(name: params[:session][:name]).try(:authenticate, params[:session][:password])
     if customer.present?
       login customer
       redirect_to customers_path
     else
       render :new
    end
  end
  
  def destroy
    logout
    redirect_to root_path
  end
end
