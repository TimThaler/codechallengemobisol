module SessionsHelper
  def current_user
    @current_user ||= BaseCustomer.find_by(id: session[:customer_id]) if session[:customer_id]
  end
end
