class Setup < ActiveRecord::Migration[5.2]
  def change
    create_table :foo_customers do |t|
      t.string :email
      t.timestamps
    end
    create_table :bar_customers do |t|
      t.string :hometown
      t.string :make
      t.string :model
      t.timestamps
    end
    create_table :base_customers do |t|
      t.string  :name
      t.string  :phone
      t.string  :password_digest
      t.integer :batt_cap
      t.string  :extension_type, null: false
      t.integer :extension_id, null: false
      t.timestamps
    end
    add_index :base_customers, [:extension_type, :extension_id], name: :index_customers_on_extension_type_and_id

  end
end
