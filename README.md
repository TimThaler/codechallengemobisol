# README

# Tasks

1. Your task is to implement a Concern (or any comparable pattern) that can be re-used in multiple models
  (i.e. in the ones given above) to encapsulate the logic necessary to store custom attributes.
2. he configuration which Model allows for which custom attribute should live in the database as it would be editable by users if we would add a configuration interface.
3. For testing imagine two partners: The first partner might only want to add email:string to their Customer
model, the second partner might want to add hometown:string to Customer as well as make:string and
model:string to Battery.
4. There is no need to model the partners themselves as they all will use individual instances of your
Rails application.
5. Users need to be able to query the custom attribute using pure SQL, so serialising custom
attributes into JSON is would be impractical.
# Solutions
* A base customer model has been choosen. This model contains the basic common customer fields, e.g. name, phone and the battery
capacity. The extension models only keep their specific information. 
* This was implemented with a polymorphic association. To add a new company there steps have to be done:
   1. Write migration file
   2. Add association in new model in the extension module

   The base model automatically loads all classes in the extension module. Which objects will be returend on specific queries
   depends on the logged in customer. This customer (which should have a specific role, which is not implemented) only gets 
   the attributes and information related to his own model. All records returned or altered are of the same extension.
   The create\_with\_extension method in the base\_customer model creates a BaseCustomer together with the extension model 
   based on the passed hash. This is demonstrated in the file script.rb.
   To test that a user only can see this records the server should be started and either per views or curl this behaviour can
   be tested.
   * 127.0.0.1/signup with either timi:test or tomi:test will redirect to the index page and show the distinct records.(the database hits are not optimised)
   * A show view shows the specific fields to a User
   * The update method is unfinished. The form should update two different objects and also autofill it. 

# Limitations/Bugs

* When two models have the exact same set of attributes this approch as is might show unintended behaviour

* ~~BUG: Auto\_loading not working for module namespaces correct. It needs to first make a call to the model to load it manually~~

