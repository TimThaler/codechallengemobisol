
@hash_a = {name: 'Bar', phone: '007', email: "hello@world.com"  }
@hash_b = {name: 'Baz', phone: '070', model: "world@hello.com", hometown: "Berlin", make:"just fine"  }

ap BaseCustomer.create_with_extension @hash_a
# => creates BaseCustomer with name and phone fields
# => creates BarCustomer with email field 

ap BaseCustomer.create_with_extension @hash_b


# => creates BaseCustomer with name and phone fields
# => creates FooCustomer with hometown email make fields

