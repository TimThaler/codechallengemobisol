Rails.application.routes.draw do
  root 'sessions#new'
  post 'signup',   to: 'sessions#create'
  get  'signup',   to: 'sessions#new'
  get  'signout',  to: 'sessions#destroy'
  resources :customers#, only: [:show, :index, :create, :new, :update]
end
