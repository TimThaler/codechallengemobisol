BaseCustomer.all.map &:destroy if BaseCustomer.count >0
3.times do |n|
  foo= Extension::FooCustomer.new
  foo.update_attributes(email: "hello@world_#{n}")
  foo.create_base_customer!(name: "timi_#{n}", phone: "00#{n}", batt_cap: 1,password: BCrypt::Password.create("test"))
end
4.times do |n|
  bar= Extension::BarCustomer.new
  bar.update_attributes(model: "model_#{n}", make: "make_#{n}", hometown: "city_#{n}")
  bar.create_base_customer!(name: "tomi_#{n}", phone: "#{n}00", batt_cap: 1,password: BCrypt::Password.create("test"))
end

@a = {name: 'Customer A', password: "test",phone: 'dummyphone', email: "email@mail.de"  }
@b = {name: 'Customer B', password: "test", phone: 'dummyphone', model: "1.1", hometown: "City", make:"just fine"  }

BaseCustomer.create_with_extension @a
BaseCustomer.create_with_extension @b


BaseCustomer.extend_base_customer("age", "int")
BaseCustomer.extend_base_customer("height", "int")
BaseCustomer.extend_base_customer("middle_name", "string")

